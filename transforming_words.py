def load_data(file_name: str) -> list:
    return [word.strip().lower() for word in open(file_name)]

def word_transform(start: str, target: str) -> list:
    words = load_data("4letters.txt")
    transformation = []

    if len(start) != len(target):
        raise ValueError("Start and target words must be of the same length")

    while start != target:
        for idx, char in enumerate(start):

            if start[idx] != target[idx]:
                new_word = start[:idx] + target[idx] + start[idx + 1:]

                if new_word in words:
                    transformation.append(f"{start} -> {new_word}")
                    start = new_word
                    break

    return transformation
    
print(word_transform("clay", "gold"))